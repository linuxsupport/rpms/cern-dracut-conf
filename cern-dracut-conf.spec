Name:       cern-dracut-conf
Version:    1.3
Release:    1%{?dist}
Summary:    CERN configuration for dracut

Group:      CERN/Utilities
License:    GPLv2
URL:        http://linux.cern.ch
Source0:    %{name}-%{version}.tgz

BuildArch: noarch
Requires: dracut
Requires(post): coreutils

%description
CERN configuration for dracut.

%prep
%setup -n %{name}-%{version}

%install
mkdir -p %{buildroot}/etc/dracut.conf.d/

install -m 644 *.conf %{buildroot}/etc/dracut.conf.d/

# This hack is here to replace the old files we created by hand. It can be removed
# once our original sin is wiped from the earth (nobody uses images created before 2022-03-01).
# This is a %post and not a %pre because %pre runs too late, once rpm has already fingerprinted the running system. (https://github.com/rpm-software-management/rpm/issues/1934)
%post
if [[ $1 -eq 1 ]]; then
    if [[ -e /etc/dracut.conf.d/mdadm.conf.rpmnew ]]; then
        mdadm=$(md5sum /etc/dracut.conf.d/mdadm.conf 2>/dev/null | cut -f1 -d' ')
        [[ "$mdadm" == "01082b5c301834feb7943b37e1f747f6" ]] && mv -fv /etc/dracut.conf.d/mdadm.conf.rpmnew /etc/dracut.conf.d/mdadm.conf
    fi
    if [[ -e /etc/dracut.conf.d/nvme.conf.rpmnew ]]; then
        nvme=$(md5sum /etc/dracut.conf.d/nvme.conf 2>/dev/null | cut -f1 -d' ')
        [[ "$nvme"  == "ea99953becb9cb205a83bbc5113852cc" ]] && mv -fv /etc/dracut.conf.d/nvme.conf.rpmnew /etc/dracut.conf.d/nvme.conf
    fi
fi
exit 0


# The following is mostly copied from the microcode-ctl package
%posttrans
# We only want to regenerate the initramfs for a fully booted
# system; if this package happened to e.g. be pulled in as a build
# dependency, it is pointless at best to regenerate the initramfs,
# and also does not work with rpm-ostree:
# https://bugzilla.redhat.com/show_bug.cgi?id=1199582
# https://bugzilla.redhat.com/show_bug.cgi?id=1530400
[ -d /run/systemd/system ] || exit 0

# We can't simply update all initramfs images, since "dracut --regenerate-all"
# generates initramfs even for removed kernels and if dracut generates botched
# initramfs image, that results in unbootable system, even with older kernels
# that can't be used as a fallback:
# https://bugzilla.redhat.com/show_bug.cgi?id=1420180
# https://access.redhat.com/support/cases/#/case/01779274
# https://access.redhat.com/support/cases/#/case/01814106
#
# ...and we can't simply limit ourselves to updating only the currently
# running kernel, as this doesn't work well with cases where kernel
# is installed before the updated microcode, or in the same transaction.
# And we can't rely on late update either, due to issues like this:
# https://bugzilla.redhat.com/show_bug.cgi?id=1710445
#
# ...and there are also issues with setups with increased "installonly_limit"
# in /etc/yum.conf, which could lead to unacceptably long package installation
# times.
#
# So, in the end, we try to grab no more than 2 most recently installed kernels
# that are installed after the currently running one (with the currently running
# kernel that makes up to 3 in total, the default "installonly_limit" value)
# as a kernel package selection heuristic that tries to accomodate both the need
# to put the latest microcode in freshly installed kernels and also addresses
# existing concerns.
#
# For RPM selection, kernel flavours (like "debug" or "kdump" or "zfcp",
# with only the former being relevant to x86 architecture) are a part or RPM
# name; it's also a part of uname, with different separator used in RHEL 6/7
# and RHEL 8.  RT kernel, however, is special, as "rt" is another part
# of RPM name and it has its own versioning scheme both in NVR and uname.
# And there's the kernel package split in RHEL 8, so one should look for *-core
# and not the main package.
pkgs="kernel-core kernel-debug-core kernel-rt-core kernel-rt-debug-core"
qf='%%{NAME} %%{VERSION}-%%{RELEASE}.%%{ARCH} %%{installtime}\n'
: "${MICROCODE_RPM_KVER_LIMIT=2}"

rpm -qa --qf "${qf}" ${pkgs} | sort -r -n -k'3,3' | {
	kver_cnt=0
	skip=0

	while read -r pkgname vra install_ts; do
		flavour=''

		# For x86, only "debug" flavour exists in RHEL 8
		[ "x${pkgname%*-debug-core}" = "x${pkgname}" ] \
			|| flavour='+debug'

		kver_cnt="$((kver_cnt + 1))"
		kver_uname="${vra}${flavour}"

		# Also check that the kernel is actually installed:
		# https://bugzilla.redhat.com/show_bug.cgi?id=1591664
		# We use the presence of symvers file as an indicator, the check
		# similar to what weak-modules script does.
		#
		# Now that /boot/symvers-KVER.gz population is now relies
		# on some shell scripts that are triggered by other shell
		# scripts (kernel-install, which is a part of systemd) that
		# called by RPM scripts, and systemd is not inclined to fix
		# https://bugzilla.redhat.com/show_bug.cgi?id=1609698
		# https://bugzilla.redhat.com/show_bug.cgi?id=1609696
		# So, we check for symvers file inside /lib/modules.
		#
		# XXX: Not sure if this check is still needed, since we now
		# iterate over the rpm output.
		[ -e "/lib/modules/${kver_uname}/symvers.gz" ] || continue
		# Check that modules.dep for the kernel is present as well,
		# otherwise dracut complains with "/lib/modules/.../modules.dep
		# is missing. Did you run depmod?".
		[ -e "/lib/modules/${kver_uname}/modules.dep" ] || continue

		# We update the kernels with the same uname as the running kernel
		# regardless of the selected limit
		if [ "x$(uname -r)" = "x${kver_uname}" \
		     -o \( "${kver_cnt}" -le "${MICROCODE_RPM_KVER_LIMIT}" \
		           -a "${skip}" = 0 \) ]
		then
            echo "Updating initramfs for kernel ${kver_uname}"
			dracut -f --kver "${kver_uname}"
		fi

		# The packages are processed until a package with the same uname
		# as the running kernel is hit (since they are sorted
		# in the descending installation time stamp older).
		[ "x$(uname -r)" != "x${kver_uname}" ] || skip=1
	done
}

exit 0


%files
%defattr(-,root,root)
%config(noreplace) /etc/dracut.conf.d/*.conf


%changelog
* Wed Mar 08 2023 Alex Iribarren <Alex.Iribarren@cern.ch> - 1.3-1
- Remove mpt3sas, not needed anymore

* Thu Feb 23 2023 Alex Iribarren <Alex.Iribarren@cern.ch> - 1.2-1
- Remove ahci_platform, add mpt3sas driver

* Wed Feb 22 2023 Alex Iribarren <Alex.Iribarren@cern.ch> - 1.1-2
- Actually add the driver

* Tue Feb 21 2023 Alex Iribarren <Alex.Iribarren@cern.ch> - 1.1-1
- Added ahci_platform driver

* Mon Feb 14 2022 Alex Iribarren <Alex.Iribarren@cern.ch> - 1.0-1
- Package configuration files
